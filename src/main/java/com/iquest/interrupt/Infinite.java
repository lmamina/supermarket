package com.iquest.interrupt;

public class Infinite extends Thread {

	public void run() {
		try {
			while (true) {
				System.out.println(this.isInterrupted());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
