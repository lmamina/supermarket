package com.iquest.interrupt;

import java.util.concurrent.TimeUnit;

public class Main {

	public static void main(String[] args) {
		Thread t = new Infinite();
		t.start();

		try {
			TimeUnit.SECONDS.sleep(1);
			t.interrupt();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
