package com.iquest.horseRace;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

public class Track {
  
  private CyclicBarrier cyclicBarrier;
  private List<Horse> horses;
  private int steps = 0;

  public Track() {
    this.cyclicBarrier = new CyclicBarrier(4, new Runnable() {

      @Override
      public void run() {
        System.out.println("-------------------------------");
        
        for(Horse horse : horses) {
          for (int i=1; i<=horse.getStep(); i++) {
            System.out.print("*");
          }
          System.out.println();
        }
        steps++;
        
        if (steps >= 40) {
          for(Horse horse : horses) {
            horse.setRunning(false);
          }
        }
        try {
          TimeUnit.MILLISECONDS.sleep(500);
        }
        catch (InterruptedException e) {
        }
      }
      
    });    
    horses = new ArrayList<Horse>();
    
    for (int i=0;i<4; i++) {
      Horse horse = new Horse(cyclicBarrier);
      horses.add(horse);
      
      new Thread(horse).start();
    }
  }

  

}
