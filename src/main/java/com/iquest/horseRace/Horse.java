package com.iquest.horseRace;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class Horse implements Runnable {

  private volatile boolean running;
  private int step;
  private CyclicBarrier cyclicBarrier;

  public Horse(CyclicBarrier cyclicBarrier) {
    this.cyclicBarrier = cyclicBarrier;
    running = true;
  }

  @Override
  public void run() {
    while (running) {
      step += new Random().nextInt(5);
      try {
        cyclicBarrier.await();
      }
      catch (InterruptedException e) {
        System.out.println("Horse interrupted!");
      }
      catch (BrokenBarrierException e) {
        System.out
            .println("Other horse was interrupted! This horse is closed.");
      }
    }
  }

  public int getStep() {
    return step;
  }

  public void setRunning(boolean running) {
    this.running = running;
  }
  
}
