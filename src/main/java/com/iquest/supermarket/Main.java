package com.iquest.supermarket;

class Main {

	public static void main(String[] args) {
		Supermarket supermarket = new Supermarket();

		Thread thread = new Thread(supermarket);
		thread.start();

		supermarket.close(7);
	}
}