package com.iquest.supermarket;

import java.util.LinkedList;
import java.util.List;

/**
 * Manages the cash registries.
 *
 * @author laura.mamina
 *
 */
public class CashRegistryManager {

  /**
   * The list with the cash registries to manage.
   */
  private List<CashRegistry> cashRegistryList;
  private boolean allCashRegistriesClosed;

  public CashRegistryManager() {
    cashRegistryList = new LinkedList<CashRegistry>();

    cashRegistryList.add(new CashRegistry(cashRegistryList.size()));
    cashRegistryList.add(new CashRegistry(cashRegistryList.size()));
    cashRegistryList.add(new CashRegistry(cashRegistryList.size()));
    cashRegistryList.add(new CashRegistry(cashRegistryList.size()));

    for (CashRegistry cashRegistry : cashRegistryList) {
      new Thread(cashRegistry).start();
    }

    allCashRegistriesClosed = false;
  }

  /**
   * Closing all the cash registries; the cash registries will not accept
   * anymore clients.
   */
  public void closeAllCashRegistries() {
    allCashRegistriesClosed = true;

    for (CashRegistry cashRegistry : cashRegistryList) {
      cashRegistry.stopRunning();
    }

    System.out.println("All cash registries closed.");
  }

  /**
   * Gets the cash registry with the minimum clients in queue and set it to not
   * be allowed to be closed.
   *
   * @return a cash registry with the minimum clients in queue
   */
  private CashRegistry getCashRegistry() {
    int minNrClients = Integer.MAX_VALUE;
    CashRegistry optimalCashRegistry = null;

    for (CashRegistry cashRegistry : cashRegistryList) {


      if (cashRegistry.isOpened()
          && cashRegistry.getNrOfClients() < minNrClients) {
        minNrClients = cashRegistry.getNrOfClients();
        optimalCashRegistry = cashRegistry;
      }
    }

    return optimalCashRegistry;
  }

  /**
   * Assigns a client to a cash registry.
   * @param client
   */
  public void addClientToACashRegistry(Client client) {
    CashRegistry cashRegistry = getCashRegistry();

    if (cashRegistry == null || cashRegistry.getNrOfClients() >= 4) {
      cashRegistry = createNewCashRegistry();
    }

    cashRegistry.addClient(client);

    System.out.println("Optimal cash registry: "
                       + cashRegistry.getCashRegistryNr() + ".             Has "
                       + cashRegistry.getNrOfClients() + " clients");
  }

  /**
   * Opens a new cash registry.
   * @return
   */
  private CashRegistry createNewCashRegistry() {
    CashRegistry cashRegistry = new CashRegistry(cashRegistryList.size());
    cashRegistryList.add(cashRegistry);
    new Thread(cashRegistry).start();

    System.out.println("New cash registry got created.");

    return cashRegistry;
  }

  public boolean isAllCashRegistriesClosed() {
    return allCashRegistriesClosed;
  }
}
