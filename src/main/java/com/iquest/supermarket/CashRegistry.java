package com.iquest.supermarket;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

/**
 * Class that simulates a cash registry.
 *
 * @author Laura
 *
 */
public class CashRegistry implements Runnable {

  private int cashRegistryNr;
  private Queue<Client> clientsQueue;
  private boolean cashRegistryOpen = false;
  private Object object;

  public CashRegistry(int cashRegistryNumber) {
    this.cashRegistryNr = cashRegistryNumber;
    clientsQueue = new LinkedList<Client>();
    object = new Object();
  }

  /**
   * Gets the number associated to this cash registry.
   *
   * @return
   */
  public int getCashRegistryNr() {
    return cashRegistryNr;
  }

  /**
   * Gets the number of clients attached to this cash registry.
   *
   * @return
   */
  public int getNrOfClients() {
    // needed because while manager gets number of clients, one client can be
    // served in that time: so it is made a poll on the queue
    synchronized (object) {
      return clientsQueue.size();
    }
  }

  /**
   * Adds a client if the cash registry is not closed.
   *
   * @param c
   *          the client to be added.
   * @return false if the cash registry is closed and the client couldn't be
   *         created or true if the client was attached to this cash registry.
   */
  public boolean addClient(Client c) {

    if (!cashRegistryOpen) {
      return false;
    }

    // needed because while manager adds a client, one client can be
    // served in that time: so it is made a poll on the queue
    synchronized (object) {
      clientsQueue.add(c);
      object.notifyAll();
    }

    System.out.println("New client added in cash registry " + cashRegistryNr
                       + ": " + c.getShoppingTime());

    return true;
  }

  /**
   * Close the cash registry.
   */
  public void stopRunning() {
    cashRegistryOpen = false;
    synchronized (object) {
      // notify the cash registries that were waiting for clients to be added in
      // the queue that now can wake up and close.
      object.notifyAll();
    }
  }

  @Override
  public void run() {
    cashRegistryOpen = true;

    while (cashRegistryOpen || !clientsQueue.isEmpty()) {
      while (clientsQueue.isEmpty() && cashRegistryOpen) {
        waitForClients();
      }

      while (!clientsQueue.isEmpty()) {
        serveClient();
      }
    }

    System.out.println("Cash registry " + cashRegistryNr + " closed.");
  }

  /**
   * Wait until a client comes to the cash registry.
   */
  private void waitForClients() {
    try {
      System.out.println("Cash registry " + cashRegistryNr + " is waiting...");

      synchronized (object) {
        object.wait();
      }

      System.out.println("Cash registry " + cashRegistryNr + " is woked up!");
    } catch (InterruptedException e) {
      System.out.println("Something happend to the cash registry "
                         + cashRegistryNr + " while waiting for the clients! ");
      Thread.currentThread().interrupt(); // needed for reseting the thread
      // interrupted flag
    }
  }

  /**
   * Serves a client.
   */
  private void serveClient() {
    try {
      System.out.println("Cash registry " + cashRegistryNr
                         + " is serving a client.");

      Client c = null;

      // needed because while manager gets number of clients, one client can be
      // served in that time: so it is made a poll on the queue
      synchronized (object) {
        c = clientsQueue.poll();
      }

      TimeUnit.MILLISECONDS.sleep(c.getShoppingTime());

      System.out.println("Cash registry " + cashRegistryNr
                         + " finished serving a client.");
    } catch (InterruptedException e) {
      System.out
          .println("Something happend with the client to the cash registry "
                   + cashRegistryNr + " while was served.");
    }
  }

  /**
   * Returns the state of the cash registry.
   *
   * @return true if the cash registry is opened or false if it is closed.
   */
  public boolean isOpened() {
    return cashRegistryOpen;
  }
}