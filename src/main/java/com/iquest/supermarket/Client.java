package com.iquest.supermarket;
import java.util.Random;

public class Client {

  private int shoppingTime;

  public Client() {
    shoppingTime = new Random().nextInt(1700);
    System.out.println("New client created: " + shoppingTime);
  }

  public int getShoppingTime() {
    return shoppingTime;
  }
}
