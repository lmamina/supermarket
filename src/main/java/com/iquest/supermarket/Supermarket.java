package com.iquest.supermarket;

import java.util.concurrent.TimeUnit;

/**
 * Simulates the queue of clients that are waiting to the cash registry.
 *
 * @author Laura
 *
 */
public class Supermarket implements Runnable {

  private CashRegistryManager cashRegistryManager;
  private boolean opened;
  private boolean allowedToClose;
  private Object object; // used for locking

  public Supermarket() {
    cashRegistryManager = new CashRegistryManager();
    object = new Object();
    opened = false;
    allowedToClose = true;
  }

  /**
   * Open the supermarket and add clients to the cash registries.
   */
  private void open() {
    System.out.println("Supermarket opened!");
    while (opened) {

      // don't allow to close the supermarket while there are clients created.
      allowedToClose = false;
      cashRegistryManager.addClientToACashRegistry(new Client());

      // if it was waiting to close the supermarket, notify it that now is
      // allowed to close it!
      allowedToClose = true;
      synchronized (object) {
        object.notifyAll();
      }

      try {
        // sleep some time before creating a new client
        TimeUnit.MILLISECONDS.sleep(100);
      } catch (InterruptedException e) {
        System.out.println("The main thread was interrupted!");
      }
    }
  }

  /**
   * Close the restaurant. This means that no more clients get created, but the
   * clients that were already created are served. If a client started to be
   * created, then it will be created and after that the supermarket can be
   * closed.
   */
  public void close() {
    synchronized (object) {
      while (!allowedToClose) {
        try {
          object.wait();
        } catch (InterruptedException e) {
          Thread.currentThread().interrupt();
        }
      }
      opened = false;
      cashRegistryManager.closeAllCashRegistries();
    }
    System.out.println("Supermarket is closing...");
  }

  public void close(int seconds) {
    try {
      TimeUnit.SECONDS.sleep(seconds);
    } catch (InterruptedException e) {
    }
    close();
  }

  @Override
  public void run() {
    opened = true;
    open();
  }
}