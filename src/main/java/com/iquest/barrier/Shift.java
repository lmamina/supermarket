package com.iquest.barrier;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public class Shift implements Runnable {

  private List<Player> players;
  private ExecutorService executor;
  private int maxPoints;

  public Shift(int maxPoints, List<Player> players, ExecutorService executor) {
    this.maxPoints = maxPoints;
    this.players = players;
    this.executor = executor;
  }

  @Override
  public void run() {
    boolean stop = false;
    System.out.println();
    System.out.println("New shift:");
    for (int i = 0; i < players.size(); i++) {
      System.out.println("Player " + (i + 1) + ": "
          + players.get(i).getPoints());
      if (players.get(i).getPoints() >= maxPoints) {
        stop = true;
      }
    }
    if (stop) {
      for (Player player : players) {
        player.setPlaying(false);
      }
      executor.shutdown();
      return;
    }
    try {
      TimeUnit.MILLISECONDS.sleep(500);
    }
    catch (InterruptedException e) {
      System.out.println("com.iquest.barrier-action sleep interrupted");
    }
  }
}