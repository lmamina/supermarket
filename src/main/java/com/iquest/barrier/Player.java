package com.iquest.barrier;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class Player implements Runnable {

	private CyclicBarrier barrier;
	private volatile int points;
	private volatile boolean playing = true;
	private static Random rand = new Random();

	public Player(CyclicBarrier barrier) {
		this.barrier = barrier;
		points = 0;
	}

	public int getPoints() {
		return points;
	}

	public void setPlaying(boolean playing) {
		this.playing = playing;
	}

	@Override
	public void run() {
		try {
			while (playing) {
				points += rand.nextInt(10);
				barrier.await();
			}
		} catch (BrokenBarrierException e) {
			playing = false;
		} catch (InterruptedException e) {
			playing = false;
		}
	}
}