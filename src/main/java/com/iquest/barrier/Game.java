package com.iquest.barrier;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Game {
  private List<Player> players;
  ExecutorService exec;

  public Game(int nrPlayers) {
    players = new ArrayList<Player>();
    exec = Executors.newFixedThreadPool(nrPlayers);

    CyclicBarrier barrier = new CyclicBarrier(nrPlayers, new Shift(50, players,
        exec));

    for (int i = 0; i < nrPlayers; i++) {
      players.add(new Player(barrier));
    }

  }

  public void startGame() {
    for (int i = 0; i < players.size(); i++) {
      exec.execute(players.get(i));
    }
  }
}
