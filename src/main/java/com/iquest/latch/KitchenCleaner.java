package com.iquest.latch;

import java.util.concurrent.CountDownLatch;

public class KitchenCleaner implements Runnable {

  private CountDownLatch latch;

  public KitchenCleaner(CountDownLatch latch) {
    this.latch = latch;
  }

  @Override
  public void run() {
    System.out.println("Kitchen clean!");
    latch.countDown();
  }

}
