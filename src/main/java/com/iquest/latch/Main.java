package com.iquest.latch;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

  public static void main(String[] args) {
    CountDownLatch latch = new CountDownLatch(3);
    ExecutorService executor = Executors.newFixedThreadPool(4);
    executor.execute(new RestaurantCloser(latch));
    executor.execute(new KitchenCleaner(latch));
    executor.execute(new RegistryChecker(latch));
    executor.execute(new RoomCleaner(latch));
    executor.shutdownNow();
  }
}
