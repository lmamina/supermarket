package com.iquest.latch;

import java.util.concurrent.CountDownLatch;

public class RegistryChecker implements Runnable {

  private CountDownLatch latch;

  public RegistryChecker(CountDownLatch latch) {
    super();
    this.latch = latch;
  }

  @Override
  public void run() {
    System.out.println("Registry OK!");
    latch.countDown();
  }
}
