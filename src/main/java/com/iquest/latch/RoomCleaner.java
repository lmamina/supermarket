package com.iquest.latch;

import java.util.concurrent.CountDownLatch;

public class RoomCleaner implements Runnable {

  private CountDownLatch latch;

  public RoomCleaner(CountDownLatch latch) {
    this.latch = latch;
  }

  @Override
  public void run() {
    System.out.println("Room clean!");
    latch.countDown();
  }

}
