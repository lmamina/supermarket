package com.iquest.latch;

import java.util.concurrent.CountDownLatch;

public class RestaurantCloser implements Runnable {

  private CountDownLatch latch;

  public RestaurantCloser(CountDownLatch latch) {
    this.latch = latch;
  }

  @Override
  public void run() {
    try {
      latch.await();
      System.out.println("Restaurant closed!");
    }
    catch (InterruptedException e) {
      System.out.println("Some action in the restaurant "
          + "couldn't be finished! Please check in the "
          + "morning all the activities!");
      System.out.println("Restaurant closed!");
    }

  }

}
